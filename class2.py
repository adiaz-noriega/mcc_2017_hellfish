#!/usr/bin/python

import sys
import math
import random

# Returns True if the given integer is prime
def isPrime(n):
    lim = int(math.sqrt(n)) + 1
    for i in xrange(2, lim):
        if n % i == 0:
            #print "%s is NOT prime (divisible by %s)" % (n, i)
            return False
    #print "%s IS prime" % (n)
    return True

# prints 2 types of message:
# 1) Whether the number is odd or even
# 2) A different message if the number is 4
# The input is taken directly from the user
def odd_or_even(num):
    if not num % 4:
        return "The number is a multiple of 4"
    elif not num % 2:
        return "The number is even"
    else:
        return "The number is odd"

# Asks the user for 2 numbers and returns whether they divide evenly
def even_divide(num, den):
    if num % den:
        return "%s does not evenly divide %s" % (den, num)
    else:
        return "%s evenly divides %s" % (den, num)

def isPal(s):
    def isPalAux(st):
        if len(st) <= 1:
            return True
        if st[0] == st[-1]:
            return isPalAux(st[1:-1])
        return False

    out = isPalAux(s)
    if out:
        return "%s IS a palindrome" % (s)
    else:
        return "%s IS NOT a palindrome" % (s)

def list_elems():
    max_num1 = random.randint(10,20)
    max_num2 = random.randint(10,20)
    a = [random.randint(0,10) for _ in range(max_num1)]
    b = [random.randint(0,10) for _ in range(max_num2)]
    print "a (sorted for simplicity): %s" % (sorted(a))
    print "b (sorted for simplicity): %s" % (sorted(b))
    print "Intersection: %s" % (set(a).intersection(set(b)))
