#!/usr/bin/python

import unittest
import sys

sys.path.append("./l2")
sys.path.append("./l3")
sys.path.append("./l4")
sys.path.append("./l5")
sys.path.append("./l9")
import p2_tests
import p8_tests
import P5Test
import Problem3Test
import Problem10Test
import TestCase1
import TestCase2
import TestCase4


if __name__ == "__main__":
    all_tests = [p2_tests.P2,
                 p8_tests.P8,
                 Problem3Test.Problem3Test,
                 Problem10Test.Problem10Test,
                 P5Test.P5Test,
                 TestCase1.TestCase1,
                 TestCase2.TestCase2,
                 TestCase4.TestCase4]
    loader = unittest.TestLoader()
    suites = []
    for test in all_tests:
        suite = loader.loadTestsFromTestCase(test)
        suites.append(suite)

    big_suite = unittest.TestSuite(suites)
    runner = unittest.TextTestRunner(verbosity=2)
    results = runner.run(big_suite)
