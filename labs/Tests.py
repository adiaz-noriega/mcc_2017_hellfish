#!/usr/bin/python

import unittest
import sys

sys.path.append("./l2/Problem1")
sys.path.append("./l2/Problem2")
sys.path.append("./l2/Problem3")
sys.path.append("./l4/p4")
sys.path.append("./l4/p5")
sys.path.append("./l4/p6")
sys.path.append("./l4/p7")
sys.path.append("./l4/p8")
sys.path.append("./l4/p9")
sys.path.append("./l4/p10")

import p1_tests
import p8_tests

if __name__ == "__main__":
    tests = [p1_tests.P1, p8_tests.TestDeterminants]
    loader = unittest.TestLoader()
    suites = []
    for test in tests:
        suite = loader.loadTestsFromTestCase(test)
        suites.append(suite)

    big_suite = unittest.TestSuite(suites)
    runner = unittest.TextTestRunner(verbosity=2)
    results = runner.run(big_suite)
