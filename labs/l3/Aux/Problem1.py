import math;
class number:
    _number = 0;


    def main(self, _number):
        self._number = _number;

    def multiple(self, _numerator, _denominator):
        return True if _numerator % _denominator == 0 else False;

    def isprime(self):
        i = 1;
        divisible = False;
        if self._number == 1: #fix for Lab3
            return False;

        while i < self._number:
            if self.multiple(self._number, i) and i != 1:
                return False;
            i += 1
        return True;

    #implement happy numbers
    def ishappy(self):
        visitednumbers = set();
        aux = self._number;
        cont = 0;
        while aux != 1 and (aux not in visitednumbers) and cont <= 8:
            visitednumbers.add(aux);
            aux = self.processhappynumber(list(str(aux)));
            cont += 1;

        if aux == 1:
            return True;
        if cont == 8:
            return False;

    def processhappynumber(self, _digits):
        totaldigits = 0;
        for x in _digits:
            totaldigits += (int(x)*int(x));
        return totaldigits;