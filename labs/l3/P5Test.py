#!/usr/bin/python

import P5
import unittest

class P5Test(unittest.TestCase):

    def testReverseLine(self):


        self.assertEqual(P5.reverseLine("hola"), "hola")
        self.assertEqual(P5.reverseLine("anita lava la tina"), "tina la lava anita")
        self.assertEqual(P5.reverseLine(""), "")
        self.assertEqual(P5.reverseLine("ADIOS mundo CRUEL"), "CRUEL mundo ADIOS")
        self.assertEqual(P5.reverseLine("CoMo EsTaS"), "EsTaS CoMo")
        self.assertEqual(P5.reverseLine("SSSzzz zzzSSS"), "zzzSSS SSSzzz")


if __name__ == '__main__':
    unittest.main()