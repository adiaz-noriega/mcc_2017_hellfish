def main():

    normalLine = raw_input('Enter a sentence: ')
    test = reverseLine(normalLine)
    print (test)

def reverseLine(lineContent):

    line = lineContent.split(' ')

    reversedLine = ""

    for i in reversed(line):
        reversedLine += i +" "

    return reversedLine.strip()

if __name__ == '__main__':
    main()