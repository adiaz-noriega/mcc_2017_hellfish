import Aux.Problem1


def main():
    preparefiles();
    processFiles();


def preparefiles():
    processor = Aux.Problem1.number();
    fprimes = open("Files/One.txt", "w");
    fhappies = open("Files/other.txt", "w");
    for i in range(1, 1001, 1):
        processor.main(i);
        if processor.isprime():
            fprimes.write(str(i) + ",")
        if processor.ishappy():
            fhappies.write(str(i) + ",");
    fprimes.close();
    fhappies.close();


def processFiles():
    cnt = 0;
    fprimes = open("Files/One.txt", "r");
    fhappies = open("Files/other.txt", "r");
    foutput = open("Files/output.txt", "w")
    lstprimes = str(fprimes.readline()).split(",");
    lsthappies = str(fhappies.readline()).split(",");
    lstoutput = list(set(lstprimes) & set(lsthappies));
    for x in lstoutput:
        foutput.write(str(x) + ",");
    fprimes.close();
    fhappies.close();
    foutput.close();



if __name__ == "__main__":
    main();