from random import randint
def main():
    playGame()

def playGame():
    globalCow = 0
    globalBull = 0
    randomN = getRandomN()
    guessSolved = 0
    nGuess = 0
    while(not guessSolved):
        nGuess +=1
        guessContent = raw_input('Guess: ')
        splitValue = guessContent.split(' ')
        cow = 0
        bull = 0
        for i in range(0,4,1):
            if(splitValue[i] == randomN[i]):
                cow += 1
                splitValue[i] = 'x'

        if(cow == 4):
            guessSolved = 1
        for i in range(0,4,1):
            if(splitValue[i] != 'x' and splitValue[i] in randomN):
                bull += 1

        globalCow += cow
        globalBull += bull
        print 'Cows:', cow, '  bull', bull

    print 'Number of guesses: ', nGuess, ' number of cows: ', globalCow, ' number of bulls: ', globalBull




def getRandomN():
    l = list()
    for i in range(0,4,1):
        l.append(str(randint(0,9)))
    return l

if __name__ == '__main__':
    main()