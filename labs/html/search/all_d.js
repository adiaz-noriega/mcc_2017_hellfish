var searchData=
[
  ['sample',['sample',['../classl4_1_1statistics_1_1_statistics.html#ad9254bf895adf26129be634c7c595ccc',1,'l4::statistics::Statistics']]],
  ['samplesize',['samplesize',['../classl4_1_1statistics_1_1_statistics.html#a86814c867d33d24eb131a3a8df17986c',1,'l4::statistics::Statistics']]],
  ['setnumber',['setnumber',['../classl4_1_1element_1_1element.html#ae2aefab2986a72153fb1ef7d0893f509',1,'l4::element::element']]],
  ['setx',['setx',['../classl4_1_1point_1_1point.html#ac1bdd93664191ec357406e9b1c533b37',1,'l4::point::point']]],
  ['sety',['sety',['../classl4_1_1point_1_1point.html#a52bd5a0e345506098f3b10142a161a7c',1,'l4::point::point']]],
  ['standardeviation',['standardeviation',['../classl4_1_1statistics_1_1_statistics.html#a42c283762877e1f128927d3e20a55d6a',1,'l4::statistics::Statistics']]],
  ['statistics',['Statistics',['../classl4_1_1statistics_1_1_statistics.html',1,'l4::statistics']]],
  ['statistics_2epy',['statistics.py',['../statistics_8py.html',1,'']]],
  ['suite',['suite',['../namespace_test_suite.html#a72b1f46fce7f8aeed23abde7dddd23b4',1,'TestSuite']]],
  ['suites',['suites',['../namespace_test_suite.html#a90200060f24ad71db8e62e76c4e51920',1,'TestSuite']]]
];
