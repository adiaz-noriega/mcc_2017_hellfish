﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class TestCase1(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(800,600)) 
        self.display.start()
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://automationpractice.com/index.php"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_case1(self):
        driver = self.driver
        driver.get(self.base_url + "/index.php?id_category=3&controller=category")
        driver.find_element_by_id("search_query_top").clear()
        driver.find_element_by_id("search_query_top").send_keys("Summer")
        driver.find_element_by_name("submit_search").click()        
        #driver.find_element_by_id("search_query_top").send_keys(Keys.RETURN)
        assert "No results were found for your search" not in driver.page_source
        
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)
        self.display.stop()

if __name__ == "__main__":
    unittest.main()

