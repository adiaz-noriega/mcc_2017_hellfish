from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Firefox()
driver.get("http://automationpractice.com/index.php")
assert "My Store" in driver.title
elem = driver.find_element_by_id("search_query_top")
elem.clear()
elem.send_keys("short")
elem.send_keys(Keys.RETURN)
assert "No results were found for your search" not in driver.page_source
driver.close()
