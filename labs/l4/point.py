class point:
    def __init__(self, x, y):
        self.x = x;
        self.y = y;

    def isvalid(self):
        return True if self.x > 0 and self.y > 0 else False;

    def setx(self, x):
        self.x = x;

    def sety(self,y):
        self.y = y;

    def getx(self):
        return self.x;

    def gety(self):
        return self.y;

    def getpoint(self):
        return self;

    def getx2(self):
        return self.getx()*self.getx();

    def gety2(self):
        return self.gety()*self.gety();

    def getxy(self):
        return self.getx()*self.gety();