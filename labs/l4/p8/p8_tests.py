#!/usr/bin/python

import unittest
from p8 import *

class TestDeterminants(unittest.TestCase):

    def test_1_by_1(self):
        expected = 1
        actual = det1([1])

        self.assertEqual(expected, actual)

    def test_2_by_2(self):
        expected = -2
        actual = det2([[1,2],[3,4]])

        self.assertEqual(expected, actual)

    def test_3_by_3(self):
        expected = 28
        actual = det3([[3, 2, -1], [2, -1, -3], [1, 3, -2]])

        self.assertEqual(expected, actual)

if __name__ == "__main__":
    unittest.main()
