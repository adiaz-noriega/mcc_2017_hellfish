#!/usr/bin/python

"""
Documentation for the Determinant module.

This module can calculate the determinant of
1x1 matrices (namely, scalars),
2x2 matrices
3x3 matrices
"""

class Matrix(object):
    """
        This class is not really implemented, I'm just doing it
        to have some documentation.
    """
    def __init__(self):
        self.param1 = 10
        self.param2 = 20

    def method1(self):
        """
            Method1
        """
        return self.param1

    def method2(self):
        """
            Method1
        """
        return self.param2

def det1(mat):
    """
        Determinant of 1x1 matrix
    """
    return mat[0]

def det2(mat):
    """
        Determinant of 2x2 matrix
    """
    x_coor = []
    y_coor = []
    for elem in mat:
        x_coor.append(elem[0])
        y_coor.append(elem[1])

    return (x_coor[0] * y_coor[1]) - (y_coor[0] * x_coor[1])

def det3(mat):
    """
        Determinant of 3x3 matrix
    """
    x_coor = []
    y_coor = []
    z_coor = []
    for elem in mat:
        x_coor.append(elem[0])
        y_coor.append(elem[1])
        z_coor.append(elem[2])

    sub1 = x_coor[0] * det2([[y_coor[1], z_coor[1]], [y_coor[2], z_coor[2]]])
    sub2 = y_coor[0] * det2([[x_coor[1], z_coor[1]], [x_coor[2], z_coor[2]]])
    sub3 = z_coor[0] * det2([[x_coor[1], y_coor[1]], [x_coor[2], y_coor[2]]])

    return sub1 - sub2 + sub3

if __name__ == "__main__":
    MAT1 = [5]
    MAT2 = [[1, 2], [3, 4]]
    MAT3 = [[3, 2, -1], [2, -1, -3], [1, 3, -2]]

    print det1(MAT1)
    print det2(MAT2)
    print det3(MAT3)
