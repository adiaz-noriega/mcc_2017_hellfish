def main ():

    listBinary = list(raw_input('Enter a list of elements: ').split(' '));
    print listBinary
    target = raw_input('Enter the number to find: ');

    print(binarySearch(listBinary, target))


def binarySearch(listN, target):

    listN.sort()

    print 'Sorted list: ', listN

    start = 0

    end = len(listN)
    count = 0

    while start < end:

        count += 1
        half = int((start + end) / 2)

        if int(listN[half]) == int(target):
            print 'Found element after ',count , 'iterations'
            return True

        elif target > listN[half]:
            start = half + 1
        elif target < listN[half]:
            end = half - 1


    print("Element not in the list")
    return False


if __name__ == "__main__":
    main()