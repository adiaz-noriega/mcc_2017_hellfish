#!/usr/bin/python

import P10
import unittest

class Problem10Test(unittest.TestCase):



    def testBinarySearch(self):
        self.assertEqual(P10.binarySearch([1,2,3,4,5,6,7], 7), True)
        self.assertEqual(P10.binarySearch([1,2,3,4,5,6,7], 10), False)

if __name__ == '__main__':
    unittest.main()