from __future__ import division;

import math;
import element;
import copy;
import quartil;


class Statistics:
    def __init__(self, _sample, points):
        self.sample = _sample;
        self.mean = 0;
        self.samplesize = 0;
        self.standardeviation = 0;
        self.mode = list();
        self.points = points;

    def computestats(self):
        self.printmean(); #OK
        self.getstandarddeviation(); #OK
        self.getmode(); #OK
        self.getquartiles(); #OK
        self.getlinearregresion();

    def getsample(self):
        return self.sample;

    def printmean(self):
        self.getpopulationmean();
        print "Mean =" + str(self.mean);

    def getpopulationmean(self):
        if self.mean == 0:
            def summation():
                total = 0;
                for x in self.sample:
                    total += x;
                return total;
            if self.getsamplesize() > 0:
                self.mean = summation()/self.getsamplesize();
            else:
                self.mean = 0;
        return self.mean;

    def getstandarddeviation(self):
        if self.standardeviation == 0:
            def getvariance():
                summation = 0
                for x in self.sample:
                    summation += ((x-self.getpopulationmean())*(x-self.getpopulationmean()))
                return summation / (self.getsamplesize() -1);
            self.standardeviation =  math.sqrt(getvariance());

        print "Standard Deviation = " + str(self.standardeviation);
        return self.standardeviation;

    def getmode(self):
        if len(self.mode) == 0:
            self.elements = list();
            maxoccurence = 0;
            #elment = element.element();
            for ix in range(0,len(self.sample),1):
                #iterate in self.elements to validate if it exists
                containelement = False;
                for z in self.elements:
                    if self.sample[ix] == z.number:
                        z.appear();
                        containelement = True;

                if not containelement:
                    self.elements.append(element.element().setnumber(self.sample[ix]));

            for x in self.elements:
                if x.apparences >= maxoccurence:
                    if x.apparences > maxoccurence:
                        self.mode = list();
                        maxoccurence = x.apparences;
                    self.mode.append(x);
        print "Mode: ";
        for x in self.mode:
            print "    " + str(x.number) + " has " + str(x.apparences) + " apparences";
        return self.mode;

    def getquartiles(self):
        sampleordered = self.sample;
        quartiles = quartil.Quartil(self.sample);
        for i in range(1,4,1):
            quartiles.getquartil(i);
        print "Quartiles: ";
        print quartiles.quartiles;
        return quartiles.quartiles;

    def getlinearregresion(self): #Linear regression in the form y=a + bx
        self.a = 0;
        self.b = 0;
        X = 0;
        Y = 0;
        X2 = 0;
        Y2 = 0;
        XY = 0;
        n = len(self.points);
        for point in self.points:
            X += point.getx();
            Y += point.gety();
            XY += point.getxy();
            X2 += point.getx2();
            Y2 += point.gety2();
        self.b = ((n*XY) - (X*Y)) / (n*X2 - X*X);
        self.a = (Y/n) - (self.b * (X/n));
        print "Linear Regression Parameters:"
        print "     a=" + str(self.a)
        print "     b=" + str(self.b)

    def getsamplesize(self):
        if self.samplesize == 0:
            total = 0
            for x in self.sample:
                total += 1;
            self.samplesize = total;
        return self.samplesize;