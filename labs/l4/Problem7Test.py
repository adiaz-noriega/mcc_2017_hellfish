#!/usr/bin/python

import Problem7
import unittest

class Problem1Test(unittest.TestCase):



    def testListWitLoop(self):

        self.assertEqual(Problem7.getUniqueItemsWithLoop([]), [])
        self.assertEqual(Problem7.getUniqueItemsWithLoop([1, 1, 34, 55, 89]), [1, 34, 55,89])
        self.assertEqual(Problem7.getUniqueItemsWithLoop([1, 1, 34, 55, 89, 1, 34, 55, 101, 85]), [1, 34, 55, 85, 89, 101])
        self.assertEqual(Problem7.getUniqueItemsWithLoop([1, 1, 34, 55, 89, 1, 34, 55, 101, 85, 85, 101]),[1, 34, 55, 85, 89, 101])
        self.assertEqual(Problem7.getUniqueItemsWithLoop(['a', 'b', 'b', 'a', 'c']), ['a', 'b', 'c'])

    def testWithSet(self):
        self.assertEqual(Problem7.getUniqueItemsWithSet([]), [])
        self.assertEqual(Problem7.getUniqueItemsWithSet([1, 1, 34, 55, 89]), [1, 34, 55, 89])
        self.assertEqual(Problem7.getUniqueItemsWithSet([1, 1, 34, 55, 89, 1, 34, 55, 101, 85]),[1, 34, 55, 85, 89, 101])
        self.assertEqual(Problem7.getUniqueItemsWithSet([1, 1, 34, 55, 89, 1, 34, 55, 101, 85, 85, 101]),[1, 34, 55, 85, 89, 101])
        self.assertEqual(Problem7.getUniqueItemsWithSet(['a', 'b', 'b', 'a', 'c']), ['a','b','c'])

if __name__ == '__main__':
    unittest.main()