#!/usr/bin/python

def naive_reverse(st):
    tmp = st.split(" ")
    s = ''
    while tmp:
        s += tmp.pop() + " "

    return s

def good_reverse(st):
    tmp = st.split(" ")
    tmp = tmp[::-1]
    tmp = " ".join(tmp)
    return tmp

if __name__ == "__main__":
    print(naive_reverse("uno dos tres cuatro cinco seis siete ocho"))
    print(good_reverse("uno dos tres cuatro cinco seis siete ocho"))

    print(naive_reverse("My name is Michelle"))
    print(good_reverse("My name is Michelle"))
