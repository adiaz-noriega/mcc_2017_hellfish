#!/usr/bin/python

import unittest
import p8

class P8(unittest.TestCase):
    def test_d1(self):
        expected = 5
        actual = p8.det1([5])
        self.assertEqual(actual, expected)

    def test_d2(self):
        expected = -2
        actual = p8.det2([[1, 2], [3, 4]])
        self.assertEqual(actual, expected)

    def test_d3(self):
        expected = 28
        actual = p8.det3([[3,2,-1],[2,-1,-3],[1,3,-2]])
        self.assertEqual(actual, expected)

if __name__ == "__main__":
    unittest.main(verbosity=2)
