import statistics;
import unittest;


class Problem9Test(unittest.TestCase):

    stats = statistics.Statistics([74, 28, 20, 51, 69, 83, 49, 66, 86, 86], [[43,99 ], [21,65 ], [25,79 ], [42, 75], [57,87], [59, 81]]);

    def testgetpopulationmean(self):
        self.assertEqual(self.stats.getpopulationmean(), 61.2);

    def testgetstandarddeviation(self):
        self.assertEqual(self.stats.getstandarddeviation(), 23.5834216724);

    def testgetmode(self):
        self.assertEqual(self.stats.getmode(), [86]);

    def testgetquartiles(self):
        self.assertEqual(self.stats.getquartiles(), [49,5, 67.5, 80.75]);

if __name__ == "__main___":
    unittest.main();