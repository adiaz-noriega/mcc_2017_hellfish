#!/usr/bin/python

"""
    Some docstring
"""

import random

if __name__ == "__main__":
    print "Welcome to cows and bulls!"
    get_list = range(10)
    get_list = [str(x) for x in get_list]

    num = 0
    while num < 1000:
        random.shuffle(get_list)
        num = int(''.join(get_list[:4]))

    print "The number: " + str(num)
    num = 1234

    lst_num = list(str(num))
    game_over = False

    while not game_over:
        cows = 0
        bulls = 0
        user_num = int(input("Enter a 4 digit number: "))
        if not int(user_num / 1000):
            print "The number must be 4 digits long!"
            continue

        lst_user_num = list(str(user_num))
        z = zip(lst_num, lst_user_num)

        my_bull = set()
        player_bull = set()

        for a, b in z:
            if a == b:
                cows += 1
            else:
                my_bull.add(a)
                player_bull.add(b)

        bulls = len(my_bull.intersection(player_bull))

        if cows != 4:
            print "Your number %s, cows %s, bulls %s" % (user_num,
                                                         cows, bulls)

        else:
            print "Your number %s. YOU WIN!" % (user_num)
            game_over = True
