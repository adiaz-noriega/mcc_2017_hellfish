#!/usr/bin/python

def det1(mat):
    return mat[0]

def det2(mat):
    x = []
    y = []
    for elem in mat:
        x.append(elem[0])
        y.append(elem[1])

    return ((x[0] * y[1]) - (y[0] * x[1]))

def det3(mat):
    x = []
    y = []
    z = []
    for elem in mat:
        x.append(elem[0])
        y.append(elem[1])
        z.append(elem[2])

    t1 = x[0] * det2([[y[1], z[1]], [y[2], z[2]]])
    t2 = y[0] * det2([[x[1], z[1]], [x[2], z[2]]])
    t3 = z[0] * det2([[x[1], y[1]], [x[2], y[2]]])

    return t1 - t2 + t3

if __name__ == "__main__":
    m1 = [5]
    m2 = [[1, 2], [3, 4]]
    m3 = [[3, 2, -1], [2, -1, -3], [1, 3, -2]]

    print(det1(m1))
    print(det2(m2))
    print(det3(m3))
