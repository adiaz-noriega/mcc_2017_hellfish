#!/usr/bin/python

def binary_search(lst, elem):
    lst.sort()
    start = 0
    end = len(lst)
    counter = 0
    while start < end:
        counter += 1
        half = int((start + end) / 2)
        if lst[half] == elem:
            print "Found element after %s tries" % (counter)
            return True
        elif elem > lst[half]:
            start = half + 1
        elif elem < lst[half]:
            end = half - 1

    print("Element not found")
    return False

if __name__ == "__main__":
    print(binary_search([2,1,5,8,3], 8))
