#!/usr/bin/python
"""
Problem 7. List Duplicates
Write a program (function!) that takes a list and returns a new list that contains all the elements of the first list minus all the duplicates.
Extras:
    Write two different functions to do this - one using a loop and constructing a list, and another using sets.
"""
import random

def getUniqueItemsWithLoop(listWithDuplicates):

    listWithoutDuplicates = []

    #for each item listWithDuplicates, search the corresponding one on listWithoutDuplicates
    for item in sorted(listWithDuplicates):
        if item not in sorted(listWithoutDuplicates):
            listWithoutDuplicates.append(item)

    return sorted(listWithoutDuplicates)


def getUniqueItemsWithSet(listWithDuplicates):

    resultSet = set()  # stores the unique numbers of list

    for item in listWithDuplicates:
        resultSet.add(item)

    return sorted(list(resultSet))

def getRandomListOfNumbers(size):
    resultNumber = []

    for x in range(1, size):
        resultNumber.append(random.randint(0, 99))

    return sorted(resultNumber)


def main():
    listOfNumbers = [1, 1, 2, 3, 3, 99, 5, 8, 13, 21, 34, 55, 99, 99]
    listOfChars = ['a', 'a', 'b','c','d','e','e','f','f']
    print 'Welcome to Problem 7: List Duplicates!'


    print('Unique items: ', getUniqueItemsWithSet(listOfNumbers ))
    print('Unique items: ', getUniqueItemsWithLoop(listOfChars ))

if __name__ == '__main__':
    main()
