import  random;


def getrandom(a=0, b=100):
    return random.randint(a, b);


def generatelistrandom(fixedsize=0, a=0, b=100, sort=False):
    elements = list();
    for i in range(0, fixedsize if fixedsize > 0 else getrandom(a,b), 1):
        elements.append(getrandom(a,b))
    if sort:
        elements.sort();
    return elements;