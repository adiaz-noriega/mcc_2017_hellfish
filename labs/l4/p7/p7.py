#!/usr/bin/python

"""
    Some docstring
"""

import random

def dedup_lst(_lst):
    """
        f1 docstring
    """
    other = []
    for elem in _lst:
        if elem not in other:
            other.append(elem)
        else:
            print "Duplicated element: " + str(elem)
    return other

def dedup_set(_lst):
    """
        f2 docstring
    """
    return set(_lst)

if __name__ == "__main__":
    LST = [random.randint(0, 20) for _ in range(20)]
    print LST
    print dedup_lst(LST)
    print dedup_set(LST)
