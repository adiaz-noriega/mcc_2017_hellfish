#!/usr/bin/python

"""
    Some docstring
"""

if __name__ == "__main__":
    PRIMES = set()
    HAPPY = set()

    with open('happy.txt', 'r') as f:
        LINES = f.readlines()

    for line in LINES:
        HAPPY.add(int(line))

    with open('primes.txt', 'r') as f:
        LINES = f.readlines()

    for line in LINES:
        PRIMES.add(int(line))

    COMMON = HAPPY.intersection(PRIMES)
    COMMON = list(COMMON)
    COMMON.sort()
    with open('problem4.out', 'w+') as f:
        for elem in COMMON:
            f.write(str(elem) + "\n")

    # Basic membership tests
    assert  7 in COMMON
    assert  13 in COMMON
    assert  19 in COMMON
    assert  23 in COMMON
    assert  167 in COMMON

    # Basic non-membership tests
    assert  2 not in COMMON
    assert  4 not in COMMON
    assert  40 not in COMMON
    assert  9 not in COMMON
    assert  21 not in COMMON
