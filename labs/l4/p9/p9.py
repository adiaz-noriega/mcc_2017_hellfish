#!/usr/bin/python

import math

class MyStats:
    def __init__(self, lst):
        assert len(lst), "Cannot work with empty lists"
        self.lst = lst

    def mean(self):
        return float(sum(self.lst))/len(self.lst)

    def var(self):
        m = self.mean()
        s = 0
        for elem in self.lst:
            s += (elem - m) ** 2

        return float(s)/len(self.lst)

    def std(self):
        return math.sqrt(self.var())

    def mode(self):
        counts = dict.fromkeys(self.lst, 0)
        for elem in self.lst:
            counts[elem] += 1

        curr_count = 0
        curr_best  = 0
        for k, v in counts.iteritems():
            if v > curr_count:
                curr_count = v
                curr_best = k

        return curr_best

    def quartile(self):
        lst = sorted(self.lst)
        print(lst)
        l = len(self.lst) + 1
        low = (l / 4) - 1
        high = (l / 4)
        print("q1: %s, %s" % (low, high))
        q1 = (lst[low] + lst[high]) / 2.0

        low = ((3 * l) / 4) - 1
        high = (3 * l) / 4
        print("q3: %s, %s" % (low, high))
        q3 = (lst[low] + lst[high]) / 2.0

        iqr = q3 - q1

        return (q1, q3, iqr)

if __name__ == "__main__":
    with open('infile2.csv', 'r') as f:
        lines = f.readlines()

    #lst = [2, 4, 4, 4, 5, 5, 7, 9]
    lst = []
    for line in lines:
        l = line.split(",")
        for elem in l:
            lst.append(float(elem))

    s = MyStats(lst)
    print("Mean: %s" % s.mean())
    print("Variance: %s: " % s.var())
    print("Standard deviation: %s" % s.std())
    print("Mode: %s" % s.mode())
    print("Quartiles: %s, %s, %s" % s.quartile())

    X = []
    Y = []
    with open('regfile.csv', 'r') as f:
        lines = f.readlines()

    n = 0
    for line in lines:
        l = line.split(",")
        x = l[0]
        y = l[1]
        X.append(int(x))
        Y.append(int(y))
        n += 1

    xy = [x * y for x,y in zip(X,Y)]
    x2 = [x**2 for x in X]
    y2 = [y**2 for y in Y]

    sx = sum(X)
    sy = sum(Y)
    sxy = sum(xy)
    sx2 = sum(x2)
    sy2 = sum(y2)

    # The regression line is of the form y = a + bx
    a = float((sy * sx2) - (sx * sxy)) / ((n * sx2) - (sx)**2)
    b = float((n * sxy) - (sx * sy)) / ((n * sx2) - (sx)**2)

    print("Linear regression: Y' = %s + %sX" % (a, b))
