import randomize;
import statistics;
import csv;
import point;

def generatefile():
    #first file
    fields = ['element'];
    fsample = open("files/sample.csv", "w");
    fwriter = csv.DictWriter(fsample, fieldnames=fields);
    fwriter.writeheader();
    for x in randomize.generatelistrandom(fixedsize=500,a=0,b=100):
        fwriter.writerow({"element": str(x)});
    fsample.close();
    # regression linear file
    fieldsreg = ['x', 'y'];
    fregsample = open("files/regression.csv", "w");
    fregwriter = csv.DictWriter(fregsample, fieldnames=fieldsreg);
    fregwriter.writeheader();
    for x in randomize.generatelistrandom(fixedsize=20, a=0, b=50):
        y = randomize.getrandom(0,50);
        fregwriter.writerow({"x": str(x), "y": str(y)});
    fregsample.close();


def getsample():
    sample = list();
    fsample = open("files/sample.csv");
    freader = csv.DictReader(fsample);
    for record in freader:
        sample.append(int(record["element"]));
    fsample.close();
    return sample;


def getpoints():
    points = list();
    fsample = open("files/regression.csv");
    freader = csv.DictReader(fsample);
    for record in freader:
        points.append(point.point(int(record["x"]),int(record["y"])));
    fsample.close();
    return points;

def main():
    generatefile();
    stats = statistics.Statistics(getsample(), getpoints());
    stats.computestats();

if __name__ == "__main__":
    main();