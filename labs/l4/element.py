class element:
    def __init__(self):
        self.number = -1;
        self.apparences = 0;

    def setnumber(self, number):
        self.number = number;
        self.appear();
        return self;

    def appear(self):
        self.apparences += 1;