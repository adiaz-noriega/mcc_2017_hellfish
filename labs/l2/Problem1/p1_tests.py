#!/usr/bin/python

import unittest
import p1

class P1(unittest.TestCase):
    def test_isPrime(self):
        expected = True
        actual = p1.isPrime(5)
        self.assertEqual(actual, expected)

        expected = True
        actual = p1.isPrime(19)
        self.assertEqual(actual, expected)

        expected = False
        actual = p1.isPrime(21)
        self.assertEqual(actual, expected)

if __name__ == "__main__":
    unittest.main()
