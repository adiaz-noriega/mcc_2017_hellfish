#!/usr/bin/python

from p3 import *

# Validate a bunch of palindromes
pal1 = "anitalavalatina"
pal2 = "radar"
pal3 = "d"
pal4 = "asddsa"
pal5 = "asdsa"
pal_tests = [pal1, pal2, pal3, pal4, pal5]

tests_passed = 0
rounds       = len(pal_tests)
for pal in pal_tests:
    print("Validating actual palindrome <%s>" % (pal))
    actual = isPal(pal)
    expected = pal + " IS a palindrome"
    if actual == expected:
        tests_passed += 1

print("Passed %s/%s tests for actual palindromes" % (tests_passed,
                                                     rounds))

# Validate a bunch of non palindromes
np1 = "Ricardo"
np2 = "Rey"
np3 = "Diez"

pal_tests = [np1, np2, np3]

tests_passed = 0
rounds       = len(pal_tests)
for pal in pal_tests:
    print("Validating non palindrome <%s>" % (pal))
    actual = isPal(pal)
    expected = pal + " IS NOT a palindrome"
    if actual == expected:
        tests_passed += 1

print("Passed %s/%s tests for actual palindromes" % (tests_passed,
                                                     rounds))
