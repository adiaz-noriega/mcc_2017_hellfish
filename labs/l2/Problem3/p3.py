#!/usr/bin/python

import sys
import math
import random

def isPal(s):
    def isPalAux(st):
        if len(st) <= 1:
            return True
        if st[0] == st[-1]:
            return isPalAux(st[1:-1])
        return False

    out = isPalAux(s)
    if out:
        return "%s IS a palindrome" % (s)
    else:
        return "%s IS NOT a palindrome" % (s)
