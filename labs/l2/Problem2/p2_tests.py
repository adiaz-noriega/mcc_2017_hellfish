#!/usr/bin/python

from p2 import *

tests_passed = 0
rounds       = 5
for i in range(rounds):
    print "##### Test %s begin #####" % (i + 1)
    a, b, c = list_elems()
    print "##### Test %s end #####" % (i + 1)
    is_in_both = True
    for elem in c:
        if elem not in a or elem not in b:
            is_in_both = False
            break
    if is_in_both:
        tests_passed += 1

print("Passed %s/%s tests" % (tests_passed, rounds))
