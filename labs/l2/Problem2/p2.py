#!/usr/bin/python

import sys
import math
import random

def list_elems():
    max_num1 = random.randint(10,20)
    max_num2 = random.randint(10,20)
    a = [random.randint(0,10) for _ in range(max_num1)]
    b = [random.randint(0,10) for _ in range(max_num2)]
    print "a (sorted for simplicity): %s" % (sorted(a))
    print "b (sorted for simplicity): %s" % (sorted(b))
    print "Intersection: %s" % (set(a).intersection(set(b)))
    return a, b, set(a).intersection(set(b))
