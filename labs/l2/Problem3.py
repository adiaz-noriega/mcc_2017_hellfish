#!/usr/bin/python

def isPalindrome(word):
    """This is about big_func1.
    
    :param abra: abra number. 
    :param cadabra: cadabra number.
    :param sesame: sesame information.
    :return ret_val: result information.
    :rtype: int.
    """
    # remove all white characters and make the comparison in uppercase
    word = word.replace(" ","").upper()

    start = 0
    end = len(word) - 1

    while start<=end:
		if word[start]!= word[end]:
			return False
		start, end = start +1, end-1
    return True

def main():
    word = raw_input("Please enter a Palindrome: ")


    result = "is a Palindrome" if isPalindrome(word) else "is NOT Palidrome"
    print word, result

if __name__ == '__main__':
    main()


