import Problem1;
import unittest;


class Problem1Test(unittest.TestCase):
    def testPar(self):
        self.assertTrue(Problem1.par(2));
        self.assertTrue(Problem1.par(11));
        self.assertTrue(Problem1.par(35));
        self.assertTrue(Problem1.par(12));
        self.assertTrue(Problem1.par(13));

    def testIsPrime(self):
        self.assertTrue(Problem1.isprime(2));
        self.assertTrue(Problem1.isprime(11));
        self.assertFalse(Problem1.isprime(35));
        self.assertFalse(Problem1.isprime(12));
        self.assertTrue(Problem1.isprime(13));

    def testMultiple(self):
        self.assertTrue(Problem1.multiple(4, 2));
        self.assertTrue(Problem1.multiple(10001, 73));
        self.assertEqual(Problem1.multiple(35, 7), True);
        self.assertTrue(Problem1.multiple(13, 1));
        self.assertTrue(Problem1.multiple(12, 5));

if __name__ == "__main___":
    unittest.main(verbosity=2);