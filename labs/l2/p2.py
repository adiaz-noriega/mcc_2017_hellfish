#!/usr/bin/python

"""
    Doc
"""

import random

def list_elems():
    """A list of elements that returns a list"""
    max_num1 = random.randint(10, 20)
    max_num2 = random.randint(10, 20)
    lst1 = [random.randint(0, 10) for _ in range(max_num1)]
    lst2 = [random.randint(0, 10) for _ in range(max_num2)]
    print "a (sorted for simplicity): %s" % (sorted(lst1))
    print "b (sorted for simplicity): %s" % (sorted(lst2))
    print "Intersection: %s" % (set(lst1).intersection(set(lst2)))
    return lst1, lst2, set(lst1).intersection(set(lst2))

def list_with_arg(list1, list2):
    """
        Doc
    """
    return set(list1).intersection(set(list2))
