#!/usr/bin/python

import unittest
import p2

class P2(unittest.TestCase):
    def test_list_intersection(self):
        l1 = [1, 2, 3, 4, 5, 6, 7]
        l2 = [3, 4, 5, 6, 8, 9, 10]
        expected = set([3,4,5,6])
        actual = p2.list_with_arg(l1, l2)
        self.assertEqual(actual, expected)

    def test_empty_intersection(self):
        l1 = [1, 2, 3, 4, 5]
        l2 = [6, 7, 8, 9, 10]
        expected = set()
        actual = p2.list_with_arg(l1,l2)
        self.assertEqual(actual, expected)

if __name__ == "__main__":
    unittest.main(verbosity=2)
