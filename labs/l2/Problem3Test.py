#!/usr/bin/python

import Problem3
import unittest

class Problem3Test(unittest.TestCase):
    def testIfPalindrome(self):
        self.assertEqual(Problem3.isPalindrome(""), True)
        self.assertEqual(Problem3.isPalindrome("s"), True)
        self.assertEqual(Problem3.isPalindrome("oso"), True)
        self.assertEqual(Problem3.isPalindrome("Anita Lava la tiNA"), True)
        self.assertEqual(Problem3.isPalindrome("AnitaPepas"), False)
        self.assertEqual(Problem3.isPalindrome("Szzs"), True)


if __name__ == '__main__':
    unittest.main()