import Samples;
import json;
import io;
import unittest;
from ddt import ddt, data, file_data

@ddt
class TestSort(unittest.TestCase):

    @file_data("lists.json")
    def test_sort(self, item):
        listSorted = item["listSorted"]; #json.loads(item["listSorted"]);
        issorted = True;
        aux = 0;
        for x in range(0, len(listSorted)):
            issorted = True;
            if x == 0:
                aux = listSorted[x];

            if aux > listSorted[x]:
                issorted= False;
        self.assertTrue(issorted);


if __name__ == "__main__":
    lists = Samples.Samples();
    data = lists.GenerateSortedLists();
    with io.open('lists.json', 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps([dict(rec) for rec in data], ensure_ascii=False)));
    unittest.main(verbosity=2);