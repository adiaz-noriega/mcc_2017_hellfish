import Samples;
import json;
import io;
from ddt import ddt, data, file_data
import unittest;


@ddt
class TestStdDev(unittest.TestCase):
    @file_data("data.json")
    def test_stdDev(self, item):
        sample = item["sample"];
        stdDev = item["stdDesviation"];
        self.assertEquals(Samples.GetStdDesviation(sample,1), stdDev);


if __name__ == "__main__":
    samples = Samples.Samples();
    data = samples.GenerateSamples();
    with io.open('data.json', 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps([dict(rec) for rec in data], ensure_ascii=False)));
    unittest.main(verbosity=2);