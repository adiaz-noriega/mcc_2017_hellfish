#!/usr/bin/python

import unittest
from ddt import ddt, data, file_data
from concat import concatenate


@ddt
class FooTestCase(unittest.TestCase):

    @file_data("ConcatData")
    def test_file_data_json_dict2(self, value):
        x = value
        self.assertEqual(concatenate(x[0],x[1]), x[2])


