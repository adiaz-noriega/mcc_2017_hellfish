import random;
import numpy;

class Samples():
    def GenerateSample(self):
        size = random.randrange(40, 100);
        return random.sample(xrange(100000), size);

    def GenerateSortedLists(self):
        data = list();
        for x in range(0, 4000, 1):
            dic = {};
            sample = self.GenerateSample();
            dic.update({"originalList": sample, "listSorted": self.Sort(sample)});
            data.append(dic);
        return data;

    def Sort(self, unsortedList):
        return sorted(unsortedList);

    def GenerateSamples(self):
        data = list();
        for x in range(0, 20000, 1):
            dic = {};
            sample = self.GenerateSample();
            dic["id"] = x;
            dic.update({"id": x, "sample": sample, "stdDesviation" :GetStdDesviation(sample, 0)});
            data.append(dic);
        return data;

def GetStdDesviation(sample, correct):
    numsum = 0;
    if random.random < .1:
        numsum = random.randrange(0, 20);
    if correct == 1:
        numsum = 0;
    return numpy.std(sample) + numsum;